<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin = Role::where('name', 'admin')->first();
    	$role_member = Role::where('name', 'teacher')->first();

    	$admin = new User();
        $admin->name = 'Administrator';
        $admin->first_name = 'Tessa';
    	$admin->last_name = 'Testarosa';
    	$admin->email = 'admin@wvsu.com';
    	$admin->password = bcrypt('secret');
    	$admin->save();

    	$admin->roles()->attach($role_admin);

    	$member = new User();
    	$member->name = 'Teacher';
        $member->first_name = 'Faye';
        $member->last_name = 'Valentine';
    	$member->email = 'teacher@wvsu.com';
    	$member->password = bcrypt('secret');
    	$member->save();
        
    	$member->roles()->attach($role_member);
    }
}
