<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('schools')->insert([

    		['name' => 'School of Information and Communications Technology','abrevation' => 'SICT'], // 1
    		['name' => 'School of Hotel and Restaurant Management','abrevation' => 'SHRM'], // 2
			['name' => 'School of Education','abrevation' => 'SEM'], // 3

		]);
		
    	DB::table('sections')->insert([
    		
    		['name' => 'A','year_level' => 1, 'school_id' => 1], // 1
    		['name' => 'B','year_level' => 1, 'school_id' => 1], // 2
    		['name' => 'C','year_level' => 1, 'school_id' => 1], // 3
    		
    		['name' => 'A','year_level' => 2, 'school_id' => 1], // 4
    		['name' => 'B','year_level' => 2, 'school_id' => 1], // 5
    		['name' => 'C','year_level' => 2, 'school_id' => 1], // 6

    		['name' => 'A','year_level' => 3, 'school_id' => 1], // 7
    		['name' => 'B','year_level' => 3, 'school_id' => 1], // 8
    		['name' => 'C','year_level' => 3, 'school_id' => 1], // 9
    		
    		['name' => 'A','year_level' => 4, 'school_id' => 1], // 10
    		['name' => 'B','year_level' => 4, 'school_id' => 1], // 11
    		['name' => 'C','year_level' => 4, 'school_id' => 1] // 12
            
    	]);
    }
}
