<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    protected $fillable = [
    	'name',
		'year_level',
		'school_id'
	];

	public function student()
	{
		return $this->hasOne(Student::class);
	}

	public function logs()
	{
		return $this->hasMany(Log::class);
	}

	public function school()
	{
		return $this->hasOne(School::class,'id','school_id');
	}
}
