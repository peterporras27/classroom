<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{

    protected $fillable = [
		'time_start',
        'time_end',
        'day',
        'room',
        'subject_id'
    ];

    public function subject()
    {
        return $this->hasOne(Subject::class,'id','subject_id');
    }

    public function school()
    {
        return $this->subject->school();
    }

    public function logs()
	{
		return $this->hasMany(Log::class);
	}
}
