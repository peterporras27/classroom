<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	protected $primaryKey = 'id';

	protected $fillable = [
		'subject_id',
		'student_id',
		'section_id',
		'schedule_id',
		'school_id',
		'present'
	];

	public function subject()
	{
      	return $this->hasOne(Subject::class,'id','subject_id');
	}

	public function student()
	{
		return $this->hasOne(Student::class,'id','student_id');
	}

	public function section()
	{
		return $this->hasOne(Section::class,'id','section_id');
	}

	public function schedule()
	{
		return $this->hasOne(Schedule::class,'id','schedule_id');
	}
}
