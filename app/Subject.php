<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    protected $fillable = [
		'name',
        'description',
        'section_id',
        'school_id',
        'user_id',
	];

	public function students()
	{
		return $this->belongsToMany(Student::class);
	}

	public function schedules()
	{
		return $this->hasMany(Schedule::class,'subject_id','id');
	}

	public function sections()
	{
		return Section::where('school_id','=',$this->school_id)->get();
	}

	public function section()
	{
		return $this->hasOne(Section::class,'id','section_id');
	}

	public function school()
	{
		return $this->hasOne(School::class,'id','school_id');
	}

	public function teacher()
	{
		return $this->hasOne(User::class,'id','user_id');
	}

	public function logs()
	{
		return $this->hasMany(Log::class);
	}

}
