<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $fillable = [
		'first_name',
		'last_name',
		'student_id',
		'school_id',
		'section_id',
	];

	public function subjects()
    {
      return $this->belongsToMany(Subject::class);
    }

	public function section()
	{
      	return $this->hasOne(Section::class,'id','section_id');
	}

	public function school()
	{
      	return $this->hasOne(School::class,'id','school_id');
	}

	public function logs()
	{
		return $this->hasMany(Log::class);
	}

	/**
    * Check one role
    * @param string $role
    */
    public function hasSubject($subject)
    {
      return null !== $this->subjects()->where('name', $subject)->first();
    }
}
