<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ( Auth::guard($guard)->check() ) {
            // Logic that determines where to send the user
            if ( $request->user()->hasRole('member') ) {
                return redirect('/member/home');
            }

            if ( $request->user()->hasRole('admin') ) {
                return redirect('/admin/home');
            }
        }

        return $next($request);
    }
}
