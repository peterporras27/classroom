<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\School;
use Validator;

class SectionController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Sections',
            'description' => 'Manage all sections.',
        );

        $this->validate = array(
            'name' => 'required|string|max:255',
            'year_level' => 'required|string|max:255',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request ) 
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $school = preg_replace('/\D/', '', $request->input('school') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $section = ($school) ? Section::where('school_id','=',$school)->paginate($show) : Section::paginate($show);
        
        $this->params['sections'] = $section;
        $this->params['perpage'] = $show;
        $this->params['schoolid'] = $school;
        $this->params['schools'] = School::all();

        return view('section.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->params['title'] = 'Register new section';
        $this->params['description'] = 'Customize and register new section.';
        $this->params['schools'] = School::all();

        return view('section.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('sections/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $user = new Section();
        $user->fill( $request->all() );
        $user->save();

        return redirect('sections')->with('success', $user->first_name .' ' . $user->last_name  . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit section Info';
        $this->params['description'] = 'Update section information.';
        $this->params['schools'] = School::all();

        $section = Section::find( $id );

        if ( ! $section ) {
            return redirect('sections')->with('warning', 'section no longer exist.');
        }

        $this->params['section'] = $section;

        return view('section.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $section = Section::find( $id );

        // double check if user exist.
        if ( ! $section ) {
            return redirect('sections')->with('error', 'section no longer exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('sections/'. $section->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $section->fill( $request->all() );
        $section->save();

        return redirect('sections')->with('success', $section->first_name .' ' . $section->last_name  . ' successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $section = Section::find( $id );

        if ( ! $section ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        } 

        $section->delete();

        return response()->json([
            'error' => false,
            'message' => 'section successfuly removed.'
        ]);
    }
}
