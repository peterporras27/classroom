<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SchoolYear;
use Validator;
use Auth;

class SchoolYearController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'School Year',
            'description' => 'School years',
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->gohome();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->params = array(
            'title' => 'Register School Year',
            'description' => 'Register a school year.',
        );

        return view('schoolyear.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validate = array(
            'year' => 'required|string|max:9',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schoolyear/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $schoolyear = SchoolYear::where( 'year','=', $request->input('year') )->first();

        if ($schoolyear) {
            return redirect('schoolyear/create')->with('error', 'School year already exist.');
        }

        $schoolyear = new SchoolYear();
        $schoolyear->fill( $request->all() );
        $schoolyear->save();

        return $this->gohome('success','School year successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->params = array(
            'title' => 'School Year',
            'description' => '',
        );

        // Validate ID

        // Check if School year exist

        return view('schoolyear.show', $this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params = array(
            'title' => 'Edit School Year',
            'description' => 'Modify this school year',
        );

        // Validate ID
        $sy = SchoolYear::find($id);

        if (!$sy) 
        {
            return $this->gohome('error','School year no longer exists.');
        }

        $this->params['schoolyear'] = $sy;

        return view('schoolyear.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate ID
        $schoolyear = SchoolYear::find($id);

        if (!$schoolyear) 
        {
            return $this->gohome();
        }

        $validate = array(
            'year' => 'required|string|max:9',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schoolyear/'.$id.'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $schoolyear->fill( $request->all() );
        $schoolyear->save();

        return $this->gohome('success','School year successfuly updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Validate ID
        $sy = SchoolYear::find($id);

        if ($sy)
        {
            $sy->delete();
        }

        return $this->gohome('success','School year successfuly removed.');
    }

    public function gohome($type='',$msg='')
    {
        if ( empty($type) && empty($msg) ) 
        {
            return redirect('admin/home');
        }

        if ( Auth::user()->hasRole('admin') ) 
        {
            return redirect('admin/home')->with($type, $msg);

        } else {

            return redirect('member/home')->with($type, $msg);
        }
    }
}







