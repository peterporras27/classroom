<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Student;
use App\Section;
use App\School;
use Validator;

class StudentController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Students',
            'description' => 'Manage all students.',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request ) 
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $sectionid = preg_replace('/\D/', '', $request->input('section') );
        $schoolid = preg_replace('/\D/', '', $request->input('school') );
        $students = Student::orderBy('last_name','asc');
        $where = array(); 
        $sections = null;

        if ($sectionid) {$where[] = ['section_id','=',$sectionid];}

        if ($schoolid) {
            $where[] = ['school_id','=',$schoolid];
            $sections = Section::where('school_id','=',$schoolid)->get();
        }

        if (count($where)) {$students->where($where);}

        $this->params['schools'] = School::all();
        $this->params['students'] = $students->paginate($show);
        $this->params['perpage'] = $show;
        $this->params['schoolid'] = $schoolid;
        $this->params['sections'] = $sections;
        $this->params['sectionid'] = $sectionid;

        return view('student.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->params['title'] = 'Register new student';
        $this->params['description'] = 'Customize and register new student.';
        $this->params['schools'] = School::all();

        return view('student.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $validate = array(
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'student_id' => 'required|string|max:255',
            'school_id' => 'required|integer',
            'section_id' => 'required|integer',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('students/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $student = new Student();
        $student->fill( $request->all() );
        $student->save();

        return redirect('students')->with('success', $student->first_name .' ' . $student->last_name  . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit Student Info';
        $this->params['description'] = 'Update student information.';

        $student = Student::find( $id );

        if ( ! $student ) {
            return redirect('students')->with('warning', 'Student no longer exist.');
        }

        $this->params['student'] = $student;
        $this->params['schools'] = School::all();
        $this->params['sections'] = Section::where('school_id','=',$student->school_id)->get();

        return view('student.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = array(
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'student_id' => 'required|string|max:255',
            'section_id' => 'required|integer',
            'school_id' => 'required|integer',
        );

        $student = Student::find( $id );

        // double check if user exist.
        if ( ! $student ) {
            return redirect('students')->with('error', 'Student no longer exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('students/'. $student->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $student->fill( $request->all() );
        $student->save();

        return redirect('students/'. $student->id .'/edit')->with('success', $student->first_name .' ' . $student->last_name  . ' successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {

        $student = Student::find( $id );

        if ( ! $student ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        $student->delete();

        return response()->json([
            'error' => false,
            'message' => 'Student successfuly removed.'
        ]);
    }
}
