<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Subject;
use App\Schedule;
use Auth;


class ScheduleController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Schedules',
            'description' => 'Manage all subject schedules.',
            'schedules' => Schedule::all(),
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        
        $this->params['schedules'] = Schedule::paginate($show);

        return view('schedules.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->params['title'] = 'Register new subject schedule';
        $this->params['description'] = 'Customize and register new subject schedule.';
        $this->params['subjects'] = Subject::all();

        return view('schedules.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = array(
            'time_start' => 'required|string|max:255',
            'time_end' => 'required|string|max:255',
            'day' => 'required|string|max:255',
            'room' => 'required|string|max:255',
            'subject' => 'required|integer'
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) 
        {
            $origin = ($request->input('origin')) ? $request->input('origin'):'schedules/create';
            
            return redirect($origin)
                ->withErrors( $validator )
                ->withInput();
        }

        $schedule = new Schedule();
        $schedule->fill( $request->all() );
        $schedule->subject_id = $request->input('subject');
        $schedule->save();

        $redirect = ($request->input('origin')) ? $request->input('origin'):'schedules';
        return redirect($redirect)->with('success', 'Schedule ' . $schedule->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit Schedule';
        $this->params['description'] = 'Update schedule information.';

        $schedule = Schedule::find( $id );

        if ( ! $schedule ) {
            return redirect('schedules')->with('warning', 'Schedule no longer exist.');
        }

        $this->params['schedule'] = $schedule;

        return view('schedules.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validate = array(
            'time_start' => 'required|string|max:255',
            'time_end' => 'required|string|max:255',
            'day' => 'required|string|max:255',
            'room' => 'required|string|max:255',
        );

        $schedule = Schedule::find( $id );

        // double check if user exist.
        if ( ! $schedule ) {
            return redirect('schedules')->with('error', 'Subject schedule does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schedules/'. $schedule->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $schedule->fill( $request->all() );
        $schedule->save();

        return redirect('subjects/'. $schedule->subject->id .'/edit')->with('success', 'Subject schedule details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block users who are not admin
        $request->user()->authorizeRoles(['admin','teacher']);

        $schedule = Schedule::find( $id );

        if ( ! $schedule ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        }

        $schedule->delete();

        return response()->json([
            'error' => false,
            'message' => 'User schedule successfuly removed.'
        ]);
    }
}
