<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\Schedule;
use App\Subject;
use App\Section;
use App\User;
use Validator;
use Auth;

class SubjectController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Subjects',
            'description' => 'Manage all subjects.',
        );

        $this->validate = array(
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'section_id' => 'required|integer',
            'school_id' => 'required|integer',
            'user_id' => 'required|integer',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request ) 
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $sectionid = preg_replace('/\D/', '', $request->input('section') );
        $school = preg_replace('/\D/', '', $request->input('school') );

        $subject = Subject::orderBy('name','asc');
        $where = array(); 
        $sections = null;

        if ($sectionid) {$where[] = ['section_id','=',$sectionid];}

        if ($school) {
            $where[] = ['school_id','=',$school];
            $sections = Section::where('school_id','=',$school)->get();
        }

        if (count($where)) {$subject->where($where);}

        $this->params['subjects'] = $subject->paginate($show);
        $this->params['perpage'] = $show;
        $this->params['schoolid'] = $school;
        $this->params['sections'] = $sections;
        $this->params['sectionid'] = $sectionid;
        $this->params['schools'] = School::all();

        return view('subject.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->params['title'] = 'Register new subject';
        $this->params['description'] = 'Customize and register new subject.';
        $this->params['schools'] = School::all();
        $this->params['proctors'] = User::whereHas('roles', function ($query) {
            $query->where('name', '=', 'teacher');
        })->get();

        return view('subject.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('subjects/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $subject = new Subject();
        $subject->fill( $request->all() );
        $subject->save();

        return redirect('subjects/'.$subject->id.'/edit')->with('success', 'Subject '. $subject->name .' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params['title'] = 'Edit subject Info';
        $this->params['description'] = 'Update subject information.';
        $this->params['schools'] = School::all();
        $this->params['schedules'] = Schedule::where('subject_id','=',$id)->paginate(10);
        $this->params['proctors'] = User::whereHas('roles', function ($query) {
            $query->where('name', '=', 'teacher');
        })->get();
        
        $subject = Subject::find( $id );

        if ( ! $subject ) {
            return redirect('subjects')->with('warning', 'subject no longer exist.');
        }

        $this->params['subject'] = $subject;

        return view('subject.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::find( $id );

        // double check if user exist.
        if ( ! $subject ) {
            return redirect('subjects')->with('error', 'subject no longer exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('subjects/'. $subject->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $subject->fill( $request->all() );
        $subject->save();

        return redirect('subjects')->with('success', $subject->first_name .' ' . $subject->last_name  . ' successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $subject = Subject::find( $id );

        if ( ! $subject ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        } 

        if ($subject->schedules->count()) {
            foreach ($subject->schedules as $sched) {
                $sched->delete();
            }
        }

        $subject->delete();

        return response()->json([
            'error' => false,
            'message' => 'subject successfuly removed.'
        ]);
    }
}
