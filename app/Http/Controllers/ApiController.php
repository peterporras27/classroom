<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Schedule;
use App\Student;
use App\Subject;
use App\Section;
use App\User;
use App\Log;

use DateTime;
use QrCode;


class ApiController extends Controller
{
    // https://www.simplesoftware.io/docs/simple-qrcode#docs-usage
    public function qr( $size = 100, $format = 'png', $key )
    {
    	$size = preg_replace('/\D/', '', $size );
    	$size = ( empty( $size ) ) ? 100: $size;
    	$formats = array('png','eps','svg');
    	$format = ( in_array($format, $formats) ) ? $format:'png';
        return QrCode::format( $format )->size( $size )->generate( $key );
    }

    public function getSections( Request $request ) {

        $return = array(
            'error' => true, 
            'message' => 'Please try again.',
            'data' => null
        );

        $schoolid = preg_replace('/\D/', '', $request->input('school_id') );

        if ($schoolid) {
            $sections = Section::where('school_id','=',$schoolid)->get();
            if(count($sections)){
                $return['data'] = $sections;
                $return['error'] = false;
                $return['message'] = 'Success';
            }
        }

        return response()->json($return);
    }

    public function getSubjects( Request $request ) {

        $return = array(
            'error' => true, 
            'message' => 'Please try again.',
            'data' => null
        );

        $sectionid = preg_replace('/\D/', '', $request->input('section_id') );

        if ($sectionid) {
            $sections = Subject::where('section_id','=',$sectionid)->get();
            if(count($sections)){
                $return['data'] = $sections;
                $return['error'] = false;
                $return['message'] = 'Success';
            }
        }

        return response()->json($return);
    }

    public function get_teachers( Request $request )
    {
        $teachers = User::all();
        $teacherList = array();

        foreach( $teachers as $teacher )
        {
            if (  $teacher->hasRole('teacher') ) {
                $teacherList[] = $teacher;
            }
        }

        $return['teachers'] = $teacherList;

        return response()->json( $return );
    }

    public function get_subjects( Request $request, $id )
    {

        $subjects = Subject::where( 'user_id','=', $id )->get();

        return response()->json( $subjects );
    }

    public function api_log( Request $request )
    {
        date_default_timezone_set('Asia/Manila');

        $response = array(
            'error' => true,
            'message' => 'Please try again',
            'data' => null,
        );

        $schedule = Schedule::find($request->input('schedule'));
        $current_day = strtolower(date('l'));
        $now = date('H:i:s');
        $onSchedule = false;

        // Find user who owns specific hash code.
        $student = Student::where([
            ['student_id', '=', $request->input('student')],
            ['section_id', '=', $schedule->subject->section->id],
        ])->first();

        // Check if student exist.
        if ( ! $student ) {
            $response['message'] = 'ERROR: Student not enrolled in this subject.';
            return response()->json( $response );
        }
        
        if ($schedule) 
        {
            $daytoday = strtolower($schedule->day);
            if ($daytoday==$current_day) 
            {
                $onSchedule = $this->timeIsBetweenSched($schedule->time_start, $schedule->time_end, $now);
            }
        }

        if ($onSchedule) 
        {
            $log = Log::where([
                ['subject_id','=',$schedule->subject->id],
                ['student_id','=',$student->id],
                ['section_id','=',$schedule->subject->section->id],
                ['schedule_id','=',$schedule->id],
                ['school_id','=',$student->school->id]
            ])->whereDate('created_at', Carbon::today())->first();

            if ($log) 
            {
                $log->present = true;
                $log->save();

                $response['error'] = false;
                $response['message'] = "SUCESS: Attendance recorded successfully.";
            }

            $response['data'] = $log;

        } else {

            $response['message'] = "ERROR: the class has ended or haven't started yet.";
        }

        return response()->json( $response );
    }

    public function timeIsBetweenSched($from, $till, $now)
    {
        date_default_timezone_set('Asia/Manila');

        $f = DateTime::createFromFormat('H:i:s', $from);
        $t = DateTime::createFromFormat('H:i:s', $till);
        $i = DateTime::createFromFormat('H:i:s', $now);
        if ($f > $t) $t->modify('+1 day');
        return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
    }

}
