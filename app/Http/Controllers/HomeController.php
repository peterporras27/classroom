<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SchoolYear;
use Carbon\Carbon;
use App\School;
use App\Subject;
use App\Section;
use App\Student;
use App\Schedule;
use App\Log;

use DateTime;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Dashboard',
            'description' => 'Deafault landing page for admin',
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        
        $this->params['years'] = SchoolYear::paginate(10);
        $this->params['schools'] = School::paginate(10);
        
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $this->params['perpage'] = $show;

        $schoolid = preg_replace('/\D/', '', $request->input('school') );
        $this->params['schoolid'] = $schoolid;
        
        $sectionid = preg_replace('/\D/', '', $request->input('section') );
        $subjectid = preg_replace('/\D/', '', $request->input('subject') );

        $logs = Log::orderBy('created_at','asc');
        $where = array(); 
        $sections = null;
        $subjects = null;

        if ($sectionid) {
            $where[] = ['section_id','=',$sectionid];
            $subjects = Subject::where('section_id','=',$sectionid)->get();
        }

        if ($schoolid) {
            $where[] = ['school_id','=',$schoolid];
            $sections = Section::where('school_id','=',$schoolid)->get();
        }

        if ($subjectid) {
            $where[] = ['subject_id','=',$subjectid];
        }

        if (count($where)) {$logs->where($where);}
        
        $this->params['logs'] = $logs->paginate($show);
        $this->params['sections'] = $sections;
        $this->params['sectionid'] = $sectionid;
        $this->params['subjects'] = $subjects;
        $this->params['subjectid'] = $subjectid;
        $this->params['schools'] = School::all();

        date_default_timezone_set('Asia/Manila');

        $this->params['schedules'] = Schedule::whereHas('subject', function($query) {
                $query->where('user_id', '=', Auth::user()->id);
            })->where('day','=', date('l'))->get();

        return view('schoolyear.index', $this->params);
    }

    public function qrscanner( $schedid )
    {
        date_default_timezone_set('Asia/Manila');

        $schedule = Schedule::find($schedid);
        $current_day = strtolower(date('l'));
        $now = date('H:i:s');
        $onSchedule = false;

        if ($schedule) 
        {
            $daytoday = strtolower($schedule->day);
            if ($daytoday==$current_day) 
            {
                $onSchedule = $this->timeIsBetweenSched($schedule->time_start, $schedule->time_end, $now);
            }
        }

        if ($onSchedule) 
        {
            // get all students
            $students = Student::where('section_id','=',$schedule->subject->section->id)->get();

            // create logs for the students
            if ($students) 
            {
                foreach ($students as $student) 
                {
                    $haslog = Log::where([
                        ['subject_id','=',$schedule->subject->id],
                        ['student_id','=',$student->id],
                        ['section_id','=',$schedule->subject->section->id],
                        ['schedule_id','=',$schedule->id],
                        ['school_id','=',$student->school->id]
                    ])->whereDate('created_at', Carbon::today())->first();
                    
                    if (!$haslog) 
                    {
                        $log = new Log();
                        $log->subject_id = $schedule->subject->id;
                        $log->student_id = $student->id;
                        $log->section_id = $schedule->subject->section->id;
                        $log->schedule_id = $schedule->id;
                        $log->school_id = $student->school->id;
                        $log->save();
                    }
                }
            }

        } else {

            return redirect('admin/home')->with('warning','Class for '.$schedule->subject->name.' has not started yet.');
        }

        $this->params = array(
            'title' => $schedule->subject->name,
            'description' => $schedule->subject->description,
            'subject' => $schedule->subject,
            'schedule' => $schedule,
        );

        return view('scanner',$this->params);
    }

    public function timeIsBetweenSched($from, $till, $now)
    {
        date_default_timezone_set('Asia/Manila');

        $f = DateTime::createFromFormat('H:i:s', $from);
        $t = DateTime::createFromFormat('H:i:s', $till);
        $i = DateTime::createFromFormat('H:i:s', $now);
        if ($f > $t) $t->modify('+1 day');
        return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
    }

    public function printQR( $key )
    {

        $this->params['student'] = Student::where('student_id','=',$key)->first();

        $this->params['key'] = $key;
        return view('qr', $this->params);
    }

    public function printReport(Request $request)
    {
        $schoolid = preg_replace('/\D/', '', $request->input('school') );
        $sectionid = preg_replace('/\D/', '', $request->input('section') );
        $subjectid = preg_replace('/\D/', '', $request->input('subject') );

        $logs = Log::orderBy('created_at','asc');
        $where = array(); 
        $section = null;
        $subject = null;
        $school = null;

        if ($sectionid) {
            $where[] = ['section_id','=',$sectionid];
            $section = Section::find($sectionid);
        }

        if ($schoolid) {
            $where[] = ['school_id','=',$schoolid];
            $school = School::find($schoolid);
        }

        if ($subjectid) {
            $where[] = ['subject_id','=',$subjectid];
            $subject = Subject::find($subjectid);
        }

        if (count($where)) {$logs->where($where);}
        
        $this->params['logs'] = $logs->get();
        $this->params['section'] = $section;
        $this->params['school'] = $school;
        $this->params['subject'] = $subject;

        return view('layouts.print', $this->params);
    }

    public function qrcodes( Request $request )
    {
        $section = preg_replace('/\D/', '', $request->input('section') );

        if ( $section ) {
            $this->params['students'] = Student::where('section_id','=',$section)->get();
        } else {
            $this->params['students'] = Student::all();
        }

        return view('qrcodes', $this->params);
    }

}
