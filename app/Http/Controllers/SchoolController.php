<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use Validator;
use Auth;

class SchoolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Schools',
            'description' => 'School or colleges',
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->gohome();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->params = array(
            'title' => 'Register School',
            'description' => 'Register a school.',
        );

        return view('schools.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validate = array(
            'abrevation' => 'required|string|max:10',
            'name' => 'required|string|max:255',
            'description' => 'required|string',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schools/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $schoolyear = new School();
        $schoolyear->fill( $request->all() );
        $schoolyear->save();

        return $this->gohome('success','School successfuly created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->params = array(
            'title' => 'School',
            'description' => '',
        );

        // Validate ID

        // Check if School year exist

        return view('schools.show', $this->params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->params = array(
            'title' => 'Edit School',
            'description' => 'Modify this school',
        );

        // Validate ID
        $sc = School::find($id);

        if (!$sc) 
        {
            return $this->gohome();
        }

        $this->params['school'] = $sc;

        return view('schools.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate ID
        $school = School::find($id);

        if (!$school) 
        {
            return $this->gohome();
        }

        $validate = array(
            'abrevation' => 'required|string|max:10',
            'name' => 'required|string|max:255',
            'description' => 'required|string',
        );

        $validator = Validator::make( $request->all(), $validate );

        if ( $validator->fails() ) {
            return redirect('schools/'.$id.'/edit')
                ->withErrors( $validator )
                ->withInput();
        }
        

        $school->fill( $request->all() );
        $school->save();

        return $this->gohome('success','School successfuly updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Validate ID
        $sc = School::find($id);

        if ($sc)
        {
            $sc->delete();
        }

        return $this->gohome('success','School successfuly removed.');
    }

    public function gohome($type='',$msg='')
    {
        if ( empty($type) && empty($msg) ) 
        {
            return redirect('admin/home');
        }

        if ( Auth::user()->hasRole('admin') ) 
        {
            return redirect('admin/home')->with($type, $msg);

        } else {

            return redirect('member/home')->with($type, $msg);
        }
    }
}
