<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if ( Auth::check() ) {
		return view('welcome');	
	} else {
		return redirect('login');
	}
});

Route::get('/scanner', function () {
	return view('scanner');	
});

Auth::routes();



// API for mobile
Route::get('api/log', 'ApiController@api_log')->name('api_log');
Route::get('qr/{size}/{format}/{key}', 'ApiController@qr')->name('qr');
Route::get('qr/print/{key}', 'HomeController@printQR')->name('print_qr');
Route::get('logs/print', 'HomeController@printReport')->name('print_report');
Route::get('qrcodes', 'HomeController@qrcodes')->name('qrcodes');

Route::get('check/{schedid}', 'HomeController@qrscanner')->name('qrscanner');

Route::get('api/teachers', 'ApiController@get_teachers')->name('get_teachers');
Route::get('api/subjects/{id}', 'ApiController@get_subjects')->name('get_subjects');

// admin routes
Route::get('admin/home', 'HomeController@index')->name('home');
Route::get('member/home', 'HomeController@index')->name('member');

// settings
Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings/profile', 'SettingsController@profile_update')->name('profile.update');
Route::post('settings/password', 'SettingsController@password_update')->name('password.update');

// AJAX Calls
Route::get('api/get/sections', 'ApiController@getSections')->name('getsections');
Route::get('api/get/subjects', 'ApiController@getSubjects')->name('getsubjects');

// Restful Controllers
Route::resources([
	'users' => 'UsersController',
	'roles' => 'RolesController',
	'students' => 'StudentController',
	'sections' => 'SectionController',
	'subjects' => 'SubjectController',
	'schedules' => 'ScheduleController',
	'schoolyear' => 'SchoolYearController',
	'schools' => 'SchoolController',
]);

