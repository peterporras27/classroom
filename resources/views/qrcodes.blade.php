<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>QR CODE</title>
	</head>
	<body>
		@foreach( $students as $student )
		
		<div style="width:200px;float:left;">
			<img src="{{ route( 'qr', [200, 'png', $student->student_id ]) }}" alt="">
			<br>
			<span style="font-size: 16px;margin-left: 35px;">{{ $student->first_name . ' ' . $student->last_name }}</span>
		</div>

		@endforeach

		<script>
			window.print();
		</script>
	</body>
</html>