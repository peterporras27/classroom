@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Student Information<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'students.update', $student->id ) }}" method="POST" role="form">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="first_name">First Name</label>
							<input type="text" value="{{ $student->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="first name">
						</div>

						<div class="form-group">
							<label for="last_name">Last Name</label>
							<input type="text" value="{{ $student->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="last name">
						</div>

						<div class="form-group">
							<label for="last_name">Student ID</label>
							<input type="text" value="{{ $student->student_id }}" name="student_id" class="form-control" id="student_id" placeholder="Student ID">
						</div>

						<hr>
						
						<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

					</div>
					<div class="col-md-6">

						<div class="form-group">
							<label for="school_id">School</label>
							<select name="school_id" id="inputschool_id" class="form-control" required="required">
								<option value="">-- Select School</option>
								@foreach( $schools as $school )
								<option value="{{ $school->id }}"{{ ($school->id==$student->school_id) ? ' selected':'' }}>{{ $school->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="section_id">Section</label>
							<select name="section_id" id="inputSection_id" class="form-control" required="required">
								<option value="">-- Select Section</option>
								@foreach( $sections as $section )
								<option value="{{ $section->id }}"{{ ($section->id==$student->section_id) ? ' selected':'' }}>{{ $section->year_level.' - '.$section->name }}</option>
								@endforeach
							</select>
						</div>

					</div>
				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('students.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
        	

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

			$('[name="school_id"]').change(function (e) { 
				e.preventDefault();
				if ($(this).val()!='') 
				{
					$('[name="section_id"]').html('<option value="">-- Select Section</option>');
					$.ajax({type: "GET", url: "{{ route('getsections') }}", data: {school_id:$(this).val()}, dataType: "json",
						success: function (response) {
							if(response.error==false){
								$.each(response.data, function (i, v) { 
									$('[name="section_id"]').append('<option value="'+v.id+'">'+v.year_level+' - '+v.name+'</option>');
								});
							}
						}
					});
				}
			});
        });
    </script>
@endsection

