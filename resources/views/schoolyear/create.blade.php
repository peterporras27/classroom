@extends('layouts.admin')

@section('content')

<div class="col-lg-4"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Create school year</h5>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					
					<form action="{{ route('schoolyear.store') }}" method="POST" role="form">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="">School year</label>
							<input type="text" name="year" class="form-control" placeholder="{{ date('Y').'-'.(date('Y')+1)  }}">
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
					
				</div>
			</div>
			
		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
    {{-- <a href="{{ route('sections.create') }}" class="btn btn-primary">Add section <i class="fa fa-plus"></i></a> --}}
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {
	
});
</script>
@endsection
