@extends('layouts.admin')

@section('content')
@if ( Auth::user()->hasRole('teacher') )
<div class="col-md-12">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Classes for today ({{ date('l') }})<small class="m-l-sm"> List of available classes for today.</small></h5>
		</div>
		<div class="ibox-content">
			@if( $schedules->count() > 0 )
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Subject</th>
							<th>Room</th>
							<th>Section</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						
						@foreach( $schedules as $schedule )
						<tr>
							<td>{{ $schedule->subject->name }}</td>
							<td>{{ $schedule->room }}</td>
							<td>{{ $schedule->subject->section->year_level.' - '.$schedule->subject->section->name }}</td>
							<td>{{ date('h:i A', strtotime($schedule->time_start)) }}</td>
							<td>{{ date('h:i A', strtotime($schedule->time_end)) }}</td>
							<td>
								<a href="{{ route('qrscanner',$schedule->id) }}" class="btn btn-sm btn-success">Check Attendance <i class="fa fa-check"></i></a>
							</td>
						</tr>
						@endforeach
						
					</tbody>
				</table>
			</div>
			@else
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>You don't have classes for today.</strong>
			</div>
			@endif
		</div>
	</div>
</div>
@endif
<div class="col-md-12">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Attendance Logs<small class="m-l-sm"> Manage all student attendance.</small></h5>
		</div>
		<div class="ibox-content">

			<form action="{{ route('home') }}" method="GET" class="form-inline" role="form">
				
				<div class="input-group">
					<div class="input-group-addon">Filter by School</div>
					<select name="school" class="form-control">
						<option value="">--- Select school</option>
						@foreach($schools as $school)
						<option value="{{ $school->id }}"{{ ($school->id==$schoolid) ? ' selected':'' }}>{{ $school->name }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="input-group">
					<div class="input-group-addon">Section</div>
					<select name="section" class="form-control">
						<option value="">--- Select section</option>
						@if($sections)
						@foreach($sections as $section)
						<option value="{{ $section->id }}"{{ ($section->id==$sectionid) ? ' selected':'' }}>{{ $section->year_level.' - '.$section->name }}</option>
						@endforeach
						@endif
					</select>
				</div>
				
				<div class="input-group">
					<div class="input-group-addon">Subjects</div>
					<select name="subject" class="form-control">
						<option value="">--- Select subject</option>
						@if($subjects)
						@foreach($subjects as $subject)
						<option value="{{ $subject->id }}"{{ ($subject->id==$subjectid) ? ' selected':'' }}>{{ $subject->name }}</option>
						@endforeach
						@endif
					</select>
				</div>
				
				<div class="input-group">
					<div class="input-group-addon">Show per page:</div>
					<input type="number" name="show" class="form-control" value="{{ $perpage }}" id="perpage" placeholder="10">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-default btn-sm">Go!</button>
					</span>
				</div>

			</form>

			<hr>

			@if( $logs->count() )

			<a target="_blank" href="{{ route('print_report',request()->except('page')) }}" class="btn btn-info btn-sm">
				Print Attendance Logs <i class="fa fa-print"></i>
			</a>
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Student</th>
							<th>School / Section</th>
							<th>Subject</th>
							<th>Room</th>
							<th>Schedule</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $logs as $log )

							<tr id="user-row-{{ $log->id }}">
								<td>{{ $log->student->first_name.' '.$log->student->last_name }}</td>
								<td>{{ $log->section->year_level.' - '.$log->section->name .' ('. $log->section->school->abrevation.')'}}</td>
								<td>{{ $log->subject->name }}</td>
								<td>{{ $log->schedule->room }}</td>
								<td>{{ date("h:i A", strtotime($log->schedule->time_start) ) .' - '.date("h:i A", strtotime($log->schedule->time_end) ) }}</td>
								<td>
									@if($log->present) 
										<span class="btn btn-primary btn-xs">Present <i class="fa fa-check"></i></span> 
									@else 
										<span class="btn btn-danger btn-xs">Absent</span> 
									@endif
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $logs->appends(request()->except('page'))->links() }}
			@else
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>No available data found.</strong>
			</div>
			@endif

		</div>
	</div>
</div>

@if ( Auth::user()->hasRole('admin') )
<!--
<div class="col-lg-4"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>School year<small class="m-l-sm"> list of all schoolyears recorded.</small></h5>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					@if( $years->count() > 0 )
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>School Year</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								
								@foreach( $years as $year )
								<tr>
									<td>{{ $year->year }}</td>
									<td>
										<a href="{{ route('schoolyear.edit', $year->id) }}" class="btn btn-xs btn-info">Edit <i class="fa fa-edit"></i></a>
									</td>
								</tr>
								@endforeach
								
							</tbody>
						</table>
					</div>
					@else
					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>No records to show</strong>
					</div>
					@endif
				</div>
			</div>
			<a href="{{ route('schoolyear.create') }}" class="btn btn-primary btn-xs">Add Schoolyear <i class="fa fa-plus"></i></a>
		</div>
	</div>
</div>
-->
<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Schools<small class="m-l-sm"> list of all schools.</small></h5>
		</div>
		<div class="ibox-content">
			<div class="row">

				<div class="col-sm-12">
					@if( $schools->count() > 0 )
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>School Name</th>
									<th>Abrevation</th>
									<th>Sections</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								
								@foreach( $schools as $school )
								<tr>
									<td>{{ $school->name }}</td>
									<td>{{ $school->abrevation }}</td>
									<td>
										<a href="{{ route('sections.index',['school'=>$school->id]) }}" class="btn btn-xs btn-primary">Sections <i class="fa fa-eye"></i></a>
									</td>
									<td>
										<a href="{{ route('schools.edit', $school->id) }}" class="btn btn-xs btn-info">Edit <i class="fa fa-edit"></i></a>
									</td>
								</tr>
								@endforeach
								
							</tbody>
						</table>
					</div>
					@else
					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>No records to show</strong>
					</div>
					@endif
				</div>
				
			</div>
			
			<a href="{{ route('schools.create') }}" class="btn btn-primary btn-xs">Add School <i class="fa fa-plus"></i></a>
			
		</div>
	</div>
</div>

@endif

@endsection

@section('action')
<div class="title-action">
    {{-- <a href="{{ route('sections.create') }}" class="btn btn-primary">Add section <i class="fa fa-plus"></i></a> --}}
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {
	var $=jQuery;

	$('[name="school"]').change(function(e){ e.preventDefault(); var val=$(this).val();
		$('[name="section"]').html('<option value="">--- Select section</option>');
		$('[name="subject"]').html('<option value="">--- Select subject</option>');
		if (val!='') { 
			$.ajax({type: "GET", url: "{{ route('getsections') }}", data: {school_id:val}, dataType: "json",
				success: function (response) {
					if(response.error==false){
						$.each(response.data, function (i, v) { 
							$('[name="section"]').append('<option value="'+v.id+'">'+v.year_level+' - '+v.name+'</option>');
						});
					}
				}
			});
		}
	});

	$('[name="section"]').change(function(e){ e.preventDefault(); var val=$(this).val();
		$('[name="subject"]').html('<option value="">--- Select subject</option>');
		if (val!='') {
			$.ajax({type: "GET", url: "{{ route('getsubjects') }}", data: {section_id:val}, dataType: "json",
				success: function (response) {
					if(response.error==false){
						$.each(response.data, function (i, v) { 
							$('[name="subject"]').append('<option value="'+v.id+'">'+v.name+'</option>');
						});
					}
				}
			});
		}
	});
});
</script>
@endsection
