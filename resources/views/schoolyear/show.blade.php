@extends('layouts.admin')

@section('content')

<div class="col-lg-4"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>School year<small class="m-l-sm"> list of all schoolyears recorded.</small></h5>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					@if( $years->count() > 0 )
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>School Year</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								
								@foreach( $years as $year )
								<tr>
									<td>{{ $year->year }}</td>
									<td><a href="#" class="btn btn-xs btn-danger">delete <i class="fa fa-close"></i></a></td>
								</tr>
								@endforeach
								
							</tbody>
						</table>
					</div>
					@else
					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>No records to show</strong>
					</div>
					@endif
				</div>
			</div>
			<a href="#" class="btn btn-primary btn-xs">Create Schoolyear <i class="fa fa-plus"></i></a>
		</div>
	</div>
</div>

<div class="col-lg-8"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Schools/Colleges<small class="m-l-sm"> list of all schools/colleges.</small></h5>
		</div>
		<div class="ibox-content">
			<div class="row">

				<div class="col-sm-12">
					
				</div>
				
			</div>
			
			<a href="#" class="btn btn-primary btn-xs">Create School/College <i class="fa fa-plus"></i></a>
			
		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
    {{-- <a href="{{ route('sections.create') }}" class="btn btn-primary">Add section <i class="fa fa-plus"></i></a> --}}
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {
	
});
</script>
@endsection
