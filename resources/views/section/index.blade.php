@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>User List<small class="m-l-sm">Manage all user accounts.</small></h5>
		</div>
		<div class="ibox-content">

			<form action="{{ route('sections.index') }}" method="GET" class="form-inline" role="form">
				
				<div class="input-group">
					<div class="input-group-addon">Filter by School</div>
					<select name="school" class="form-control">
						<option value="">--- Select school</option>
						@foreach($schools as $school)
						<option value="{{ $school->id }}"{{ ($schoolid==$school->id) ? ' selected':'' }}>{{ $school->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="input-group">
					<div class="input-group-addon">Show per page:</div>
					<input type="number" name="show" class="form-control" value="{{ $perpage }}" id="perpage" placeholder="10">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-default btn-sm">Go!</button>
					</span>
				</div>

			
			</form>

			<hr>
			
			@if( $sections->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Year Level</th>
							<th>School</th>
							<th>Subjects</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $sections as $section )

							<tr id="user-row-{{ $section->id }}">
								<td>{{ $section->name }}</td>
								<td>{{ $section->year_level }}</td>
								<td>{{ ($section->school) ? $section->school->name:'' }}</td>
								<td>
									<a href="{{ route('subjects.index',['section'=>$section->id]) }}" class="btn btn-info btn-xs">
										<i class="fa fa-eye"></i> View Subjects
									</a>
								</td>
								<td>
									<a href="{{ route('sections.edit', $section->id) }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a>
									<button data-id="{{ $section->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Delete
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $sections->appends(request()->except('page'))->links() }}

			@else 
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>No records found.</strong>
				</div>
			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove section</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this section and all data affiliated to this account will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('sections.create') }}" class="btn btn-primary">Add section <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var user_id = button.data('id');
		jQuery('#remove-btn').data('id',user_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var user_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('sections.index') }}/'+user_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if (data.error) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+user_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
