@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add section<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('sections.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Section Name">
						</div>

						<div class="form-group">
							<label for="year_level">Year Level</label>
							<input type="number" value="{{ old('year_level') }}" name="year_level" class="form-control" id="year_level" placeholder="Year level">
						</div>

						<div class="form-group">
							<label for="school_id">School</label>
							<select value="{{ old('school_id') }}" name="school_id" class="form-control" id="school_id">
							@foreach( $schools as $school )
							<option value="{{ $school->id }}">{{ $school->name }}</option>
							@endforeach
							</select>
						</div>

						<hr>
						
						<button type="submit" class="btn btn-primary">Add Section <i class="fa fa-save"></i></button>

					</div>
				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('sections.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection