@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Attendance Logs<small class="m-l-sm">Manage all user class logs.</small></h5>
		</div>
		<div class="ibox-content">
			
			<div class="row">

				<div class="col-md-3">
					
					<form method="GET" class="form-inline" role="form">
						
						<div class="input-group">
							<div class="input-group-addon">Per page:</div>
							<input type="number" name="show" class="form-control" value="{{ $perpage }}" id="perpage" placeholder="10">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-default">Go!</button>
							</span>
						</div>
					
					</form>

				</div>
				<div class="col-md-9">
					 
					<form action="" method="GET" class="form-inline pull-right" role="form">

					 	<div class="form-group">
					 		<label class="sr-only" for="">Date</label>
					 		<select type="number" name="day" class="form-control">
					 			<option value="">Day</option>
					 			@for( $d = 1; $d<=31; $d++ )
					 			<option value="{{ $d }}"{{ (isset($_GET['day']) && $_GET['day'] == $d ) ? ' selected':''}}>{{ $d }}</option>
					 			@endfor
					 		</select>
					 	</div>

					 	<div class="form-group">
					 		<label class="sr-only" for="">Month</label>
					 		<select name="month" class="form-control">
					 			<option value="">Month</option>
					 			@foreach( $month as $m => $n )
					 			<option value="{{ $m }}"{{ (isset($_GET['month']) && $_GET['month'] == $m ) ? ' selected':''}}>{{ $n }}</option>
					 			@endforeach
					 		</select>
					 	</div>

					 	<div class="form-group">
			        		<label class="sr-only" for="">Year</label>
			        		<select name="year" class="form-control">
			        			<option value="">Year</option>
		        				<?php
		        				$prev_year = date('Y') - 10;
		        				$next_year = date('Y') + 10;
		        				for ($year = $prev_year; $year <= $next_year; $year++) {
		        					$yr = (isset($_GET['year']) && $_GET['year'] == $year ) ? ' selected':'';
		        					echo '<option value="'.$year.'"'.$yr.'>'.$year.'</option>';
		        				}
		        				?>
							</select> 
			        	</div>

					 	<div class="form-group">
					 		<label class="sr-only" for="">Section</label>
					 		<select name="section" class="form-control" id="" placeholder="Input field">
					 			<option value="">Section</option>
					 			@foreach( $sections as $section )
					 			<option value="{{ $section->id }}"{{ (isset($_GET['section']) && $_GET['section'] == $section->id ) ? ' selected':''}}>{{ $section->year_level . '-' . $section->name }}</option>
					 			@endforeach
					 		</select>
					 	</div>
					 	
					 	<div class="form-group">
					 		<label class="sr-only" for="">Subject</label>
					 		<select name="subject" class="form-control" id="" placeholder="Input field">
					 			<option value="">Subject</option>
					 			@foreach( $subjects as $subject )
					 			<option value="{{ $subject->id }}"{{ (isset($_GET['subject']) && $_GET['subject'] == $subject->id ) ? ' selected':''}}>{{ $subject->name }}</option>
					 			@endforeach
					 		</select>
					 	</div>
					 
					 	<button type="submit" class="btn btn-primary">Filter</button>
					 	<a href="{{ route('home') }}?section=&month=&day=&year=&subject=" class="btn btn-warning">clear</a>
					 	<a href="#" id="print" class="btn btn-info">Print <i class="fa fa-print"></i></a>
					 </form>
 					
				</div>

			</div>
			
			<hr>
			
			@if( $logs->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Subject</th>
							<th>Room</th>
							<th>Student</th>
							<th>Date</th>
							<th>Section</th>
							<th>Login Time</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $logs as $log )

							<?php 

							$log_h = date('h', strtotime($log->created_at));
							$log_m = date('i', strtotime($log->created_at));

							$class_h = date('h', strtotime($log->schedule()->time_start));
							$class_m = date('i', strtotime($log->schedule()->time_start));

							$logm = $class_m + 15;

							if ( $log_h == $class_h ) {

								$islate = ( $log_m > $logm ) ? 'Tardy' : 'On Time';
								$islateclass = ( $log_m > $logm ) ? 'warning':'success';

							} else {

								$islate = 'Tardy';
								$islateclass = 'warning';
							}
							
							?>
							<tr id="user-row-{{ $log->id }}" class="{{ $islateclass }}">
								<td>{{ $log->subject()->name }}</td>
								<td>{{ $log->schedule()->room }} - ( {{ date('h:i a', strtotime($log->schedule()->time_start)) . ' - ' . date('h:i a', strtotime($log->schedule()->time_end)) }} )</td>
								<td>{{ $log->student()->first_name . ' ' . $log->student()->last_name }}</td>
								<td>{{ date('F d, Y', strtotime($log->created_at)) }}</td>
								<td>{{ $log->section()->year_level . '-' . $log->section()->name }}</td>
								<td>{{ date('h:i a', strtotime($log->created_at)) }}</td>
								<td>{{ $islate }}</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $logs->links() }}

			@else 
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>No records found.</strong>
				</div>
			@endif

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
    {{-- <a href="{{ route('sections.create') }}" class="btn btn-primary">Add section <i class="fa fa-plus"></i></a> --}}
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {
	jQuery('#print').click(function(event) {
		event.preventDefault();
		var section = jQuery('[name="section"]').val();
		var subject = jQuery('[name="subject"]').val();
		var month = jQuery('[name="month"]').val();
		var day = jQuery('[name="day"]').val();
		var year = jQuery('[name="year"]').val();

    	window.open('{{ route('print_report') }}?section='+section+'&month='+month+'&day='+day+'&year='+year+'&subject='+subject,'_blank');
		
	});
});
</script>
@endsection
