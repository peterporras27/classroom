@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Subject Schedule List<small class="m-l-sm">Manage all subject schedules.</small></h5>
		</div>
		<div class="ibox-content">
			
			@if( $schedules->count() )
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>Day</th>
							<th>Room</th>
							<th>Time Start</th>
							<th>Time End</th>
							<th>Subject</th>
							<th>School / Section</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $schedules as $schedule )

							<tr id="user-row-{{ $schedule->id }}">
								<td>{{ $schedule->day }}</td>
								<td>{{ $schedule->room }}</td>
								<td>{{ date("h:i a", strtotime($schedule->time_start) ) }}</td>
								<td>{{ date("h:i a", strtotime($schedule->time_end) ) }}</td>
								<td>{{ $schedule->subject->name }}</td>
								<td>{{ $schedule->subject->section->name.' - '.$schedule->subject->section->year_level.' ('.$schedule->school->abrevation.')' }}</td>
								<td>
									<a href="{{ url("schedules/{$schedule->id}/edit") }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i> Edit
									</a>
									<button data-id="{{ $schedule->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
										<i class="fa fa-close"></i> Remove
									</button>
								</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
			
			{{ $schedules->appends(request()->except('page'))->links() }}
			@else
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>No available data found.</strong>
			</div>
			@endif

		</div>
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" schedule="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Schedule</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this schedule will no longer be available to all users.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('schedules.create') }}" class="btn btn-primary">Add Schedule <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var schedule_id = button.data('id');
		jQuery('#remove-btn').data('id',schedule_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+schedule_id+' td:first').html() );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var schedule_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('schedules.index') }}/'+schedule_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if ( data.error ) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+schedule_id).remove();
			}
		});
		 
	});

});
</script>
@endsection
