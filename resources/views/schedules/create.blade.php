@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add Schedule<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('schedules.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-6">
						<input type="hidden" name="name" value="">
						@if( $subjects )
						<div class="form-group">
							<label for="user_role">Select Subject:</label><br>
							<select name="subject" data-placeholder="Select subject..." class="chosen-select form-control" style="width:350px;" tabindex="4">
								<option value="">-- Select Subject</option>
								@foreach( $subjects as $subject )
									<option value="{{ $subject->id }}">{{ $subject->name.' (  '.$subject->school->abrevation.' - '.$subject->section->year_level.' - '.$subject->section->name.' )' }}</option>
								@endforeach
							</select>
						</div>
						@endif

						<div class="form-group">
							<label for="room">Class Room</label>
							<input type="text" value="{{ old('room') }}" name="room" class="form-control" id="room" placeholder="Class Room">
						</div>

						<div class="form-group">
							<label for="day">Day</label>
							<select value="{{ old('day') }}" name="day" class="form-control" id="day">
								<option value="Monday"{{ ( old('day') == 'Monday' ? ' selected':'' ) }}>Monday</option>
								<option value="Tuesday"{{ ( old('day') == 'Tuesday' ? ' selected':'' ) }}>Tuesday</option>
								<option value="Wednesday"{{ ( old('day') == 'Wednesday' ? ' selected':'' ) }}>Wednesday</option>
								<option value="Thursday"{{ ( old('day') == 'Thursday' ? ' selected':'' ) }}>Thursday</option>
								<option value="Friday"{{ ( old('day') == 'Friday' ? ' selected':'' ) }}>Friday</option>
							</select>
						</div>

						<div class="form-group">
							<label for="time_start">Start time</label>
							<input type="time" value="{{ old('time_start') }}" name="time_start" class="form-control" id="time_start">
						</div>

						<div class="form-group">
							<label for="time_end">Dismissal time</label>
							<input type="time" value="{{ old('time_end') }}" name="time_end" class="form-control" id="time_end">
						</div>
	
						<button type="submit" class="btn btn-primary">Save Schedule <i class="fa fa-save"></i></button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('schedules.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script>
        jQuery(document).ready(function () {

	    	var config = {
	            '.chosen-select'           : {},
	            '.chosen-select-deselect'  : {allow_single_deselect:true},
	            '.chosen-select-no-single' : {disable_search_threshold:10},
	            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	            '.chosen-select-width'     : {width:"100%"}
	        }
	        for (var selector in config) {
	            $(selector).chosen(config[selector]);
	        }

	        $('[name="subject"]').chosen().change(function(){
	        	$('[name="name"]').val( $('option[value="'+$(this).val()+'"]').text() );
	        });

            jQuery('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            jQuery('#username').keyup(function(e) {
            	var content = jQuery(this).val().replace(/\s+/g, '-').toLowerCase();
            	jQuery(this).val(content);
            });
        });
    </script>
@endsection