@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Details<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'schedules.update', $schedule->id ) }}" method="POST" schedule="form">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col-md-6">

						<div class="form-group">
							<label for="room">Subject</label>
							<input type="text" value="{{ $schedule->subject->name }}" class="form-control" disabled>
						</div>

						<div class="form-group">
							<label for="room">Class Room</label>
							<input type="text" value="{{ $schedule->room }}" name="room" class="form-control" id="room" placeholder="Class Room">
						</div>

						<div class="form-group">
							<label for="day">Day</label>
							<select name="day" class="form-control" id="day">
								<option value="Monday"{{ ( $schedule->day == 'Monday' ? ' selected':'' ) }}>Monday</option>
								<option value="Tuesday"{{ ( $schedule->day == 'Tuesday' ? ' selected':'' ) }}>Tuesday</option>
								<option value="Wednesday"{{ ( $schedule->day == 'Wednesday' ? ' selected':'' ) }}>Wednesday</option>
								<option value="Thursday"{{ ( $schedule->day == 'Thursday' ? ' selected':'' ) }}>Thursday</option>
								<option value="Friday"{{ ( $schedule->day == 'Friday' ? ' selected':'' ) }}>Friday</option>
								<option value="Saturday"{{ ( $schedule->day == 'Saturday' ? ' selected':'' ) }}>Saturday</option>
							</select>
						</div>

						<div class="form-group">
							<label for="time_start">Start time</label>
							<input type="time" value="{{ $schedule->time_start }}" name="time_start" class="form-control" id="time_start" placeholder="7:30am">
						</div>

						<div class="form-group">
							<label for="time_end">Dismissal time</label>
							<input type="time" value="{{ $schedule->time_end }}" name="time_end" class="form-control" id="time_end" placeholder="7:30am">
						</div>

						<button type="submit" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('schedules.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
        	
        	var config = {
	            '.chosen-select'           : {},
	            '.chosen-select-deselect'  : {allow_single_deselect:true},
	            '.chosen-select-no-single' : {disable_search_threshold:10},
	            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	            '.chosen-select-width'     : {width:"100%"}
	        }
	        for (var selector in config) {
	            $(selector).chosen(config[selector]);
	        }

            jQuery('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            jQuery('#username').keyup(function(e) {
            	var content = jQuery(this).val().replace(/\s+/g, '-').toLowerCase();
            	jQuery(this).val(content);
            });
        });
    </script>
@endsection

