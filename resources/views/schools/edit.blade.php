@extends('layouts.admin')

@section('content')

<div class="col-lg-4"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Create school</h5>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-sm-12">
					
					<form action="{{ route('schools.update', $school->id) }}" method="POST" role="form">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="PUT">
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" name="name" value="{{ $school->name }}" class="form-control" placeholder="School of ">
						</div>
						<div class="form-group">
							<label for="">Abrevation</label>
							<input type="text" name="abrevation" value="{{ $school->abrevation }}" class="form-control" placeholder="ICT">
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<textarea name="description" class="form-control" placeholder="">{{ $school->description }}</textarea>
						</div>
						<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
					</form>

					<form style="float: right;margin-top: -40px;" action="{{ route('schools.destroy', $school->id) }}" method="POST" role="form">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">
						<button type="submit" class="btn btn-danger">Delete <i class="fa fa-close"></i></button>
					</form>
					
				</div>
			</div>
			
		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
    {{-- <a href="{{ route('sections.create') }}" class="btn btn-primary">Add section <i class="fa fa-plus"></i></a> --}}
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {
	
});
</script>
@endsection
