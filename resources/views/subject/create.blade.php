@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add subject<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route('subjects.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<div class="row">

					<div class="col-md-6">

						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Subject Name">
						</div>

						<div class="form-group">
							<label for="description">Description</label>
							<textarea name="description" rows="5" class="form-control" id="description" placeholder="Description">{{ old('description') }}</textarea>
						</div>

						<hr>
						
						<button type="submit" class="btn btn-primary">Add Subject <i class="fa fa-save"></i></button>

					</div>
					<div class="col-md-6">
					
						<div class="form-group">
							<label for="school_id">School</label>
							<select name="school_id" class="form-control" id="school_id">
								<option value="">--- Select School</option>
								@foreach( $schools as $school )
								<option value="{{ $school->id }}">{{ $school->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="section_id">Section</label>
							<select name="section_id" class="form-control" id="section_id">
								<option value="">--- Select section</option>
							</select>
						</div>

						<div class="form-group">
							<label for="user_id">Proctor</label>
							<select name="user_id" class="form-control" id="user_id">
								<option value="">--- Select Proctor</option>
								@foreach( $proctors as $proctor )
								<option value="{{ $proctor->id }}">{{ $proctor->first_name.' '.$proctor->last_name }}</option>
								@endforeach
							</select>
						</div>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('subjects.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script>
        $(document).ready(function () {

        	/* 
			var config = {
	            '.chosen-select'           : {},
	            '.chosen-select-deselect'  : {allow_single_deselect:true},
	            '.chosen-select-no-single' : {disable_search_threshold:10},
	            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	            '.chosen-select-width'     : {width:"100%"}
	        }
	        for (var selector in config) {
	            $(selector).chosen(config[selector]);
	        } 
			*/

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

			$('[name="school_id"]').change(function(e){
				$('[name="section_id"]').html('');
				$.ajax({type: "GET", url: "{{ route('getsections') }}", data: {school_id:$(this).val()}, dataType: "json",
					success: function (response) {
						if(response.error==false){
							$.each(response.data, function (i, v) { 
								$('[name="section_id"]').append('<option value="'+v.id+'">'+v.name+' - '+v.year_level+'</option>');
							});
						}
					}
				});
			});
        });
    </script>
@endsection