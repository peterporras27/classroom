@extends('layouts.admin')

@section('content')

<div class="col-lg-12"> 
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Edit Subject<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">
			
			<form action="{{ route( 'subjects.update', $subject->id ) }}" method="POST" role="form">

				{{ csrf_field() }}

				<input type="hidden" name="_method" value="PUT">
				
				<div class="row">

					<div class="col-md-6">

						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" value="{{ $subject->name }}" name="name" class="form-control" id="name" placeholder="Section Name">
						</div>

						<div class="form-group">
							<label for="description">Description</label>
							<textarea name="description" rows="5" class="form-control" id="description" placeholder="Description">{{ $subject->description }}</textarea>
						</div>

						<hr>

						<button type="submit" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>

					</div>
					<div class="col-md-6">

						<div class="form-group">
							<label for="school_id">School</label>
							<select name="school_id" class="form-control" id="school_id">
								<option value="">--- Select School</option>
								@foreach( $schools as $school )
								<option value="{{ $school->id }}"{{ ($subject->school_id==$school->id) ? ' selected':'' }}>{{ $school->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="section_id">Section</label>
							<select name="section_id" class="form-control" id="section_id">
								@foreach( $subject->sections() as $section )
								<option value="{{ $section->id }}"{{ ( $subject->section_id==$section->id ) ? ' selected':'' }}>{{ $section->name.' - '.$section->year_level }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="user_id">Proctor</label>
							<select name="user_id" class="form-control" id="user_id">
								<option value="">--- Select Proctor</option>
								@foreach( $proctors as $proctor )
								<option value="{{ $proctor->id }}"{{ ( $subject->user_id==$proctor->id ) ? ' selected':'' }}>{{ $proctor->first_name.' '.$proctor->last_name }}</option>
								@endforeach
							</select>
						</div>

					</div>

				</div>
			
			</form>

		</div>
	</div>
</div>
<div class="col-md-4">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Add Schedule<small class="m-l-sm"></small></h5>
		</div>
		<div class="ibox-content">

			<form action="{{ route('schedules.store') }}" method="POST" role="form">
				{{ csrf_field() }}

				<input type="hidden" name="origin" value="{{ route( 'subjects.edit', $subject->id ) }}">
				<input type="hidden" name="subject" value="{{ $subject->id }}">

				<div class="form-group">
					<label for="room">Class Room</label>
					<input type="text" value="{{ old('room') }}" name="room" class="form-control" id="room" placeholder="Class Room">
				</div>

				<div class="form-group">
					<label for="day">Day</label>
					<select value="{{ old('day') }}" name="day" class="form-control" id="day">
						<option value="Monday"{{ ( old('day') == 'Monday' ? ' selected':'' ) }}>Monday</option>
						<option value="Tuesday"{{ ( old('day') == 'Tuesday' ? ' selected':'' ) }}>Tuesday</option>
						<option value="Wednesday"{{ ( old('day') == 'Wednesday' ? ' selected':'' ) }}>Wednesday</option>
						<option value="Thursday"{{ ( old('day') == 'Thursday' ? ' selected':'' ) }}>Thursday</option>
						<option value="Friday"{{ ( old('day') == 'Friday' ? ' selected':'' ) }}>Friday</option>
						<option value="Saturday"{{ ( old('day') == 'Saturday' ? ' selected':'' ) }}>Saturday</option>
					</select>
				</div>

				<div class="form-group">
					<label for="time_start">Start time</label>
					<input type="time" value="{{ old('time_start') }}" name="time_start" class="form-control" id="time_start">
				</div>

				<div class="form-group">
					<label for="time_end">Dismissal time</label>
					<input type="time" value="{{ old('time_end') }}" name="time_end" class="form-control" id="time_end">
				</div>

				<button type="submit" class="btn btn-primary">Save Schedule <i class="fa fa-save"></i></button>

			</form>

		</div>
	</div>
</div>
<div class="col-md-8">

	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Schedules</h5>
		</div>
		<div class="ibox-content">

			@if( $schedules->count() )
			
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>Room</th>
								<th>Day</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Options</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $schedules as $sched )

								<tr id="user-row-{{ $sched->id }}">
									<td>{{ $sched->room }}</td>
									<td>{{ $sched->day }}</td>
									<td>{{ date('h:i A', strtotime($sched->time_start)) }}</td>
									<td>{{ date('h:i A', strtotime($sched->time_end)) }}</td>
									<td>
										<a href="{{ route('schedules.edit',$sched->id) }}" class="btn btn-success btn-xs">
											<i class="fa fa-edit"></i> Edit Schedule
										</a>
										<button data-id="{{ $sched->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
											<i class="fa fa-close"></i> Delete
										</button>
									</td>
								</tr>

							@endforeach
						</tbody>
					</table>
				</div>
				
				{{ $schedules->appends(request()->except('page'))->links() }}

			@else 
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>No records found.</strong>
				</div>
			@endif
		</div>
	</div>

</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove schedule</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this action cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-left btn btn-white"  data-dismiss="modal">Cancel</button>
                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('subjects.index') }}" class="btn btn-white"><i class="fa fa-chevron-left"></i> Back</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
	$(document).ready(function () {

		$('.i-checks').iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
		});

		$('[name="school_id"]').change(function(e){
			$('[name="section_id"]').html('');
			$.ajax({type: "GET", url: "{{ route('getsections') }}", data: {school_id:$(this).val()}, dataType: "json",
				success: function (response) {
					if(response.error==false){
						$.each(response.data, function (i, v) { 
							$('[name="section_id"]').append('<option value="'+v.id+'">'+v.name+' - '+v.year_level+'</option>');
						});
					}
				}
			});
		});

		toastr.options = {
			"closeButton": true,
			"debug": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"onclick": null,
			"showDuration": "400",
			"hideDuration": "1000",
			"timeOut": "7000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		jQuery('button[data-name="delete"]').click(function(e) {
			e.preventDefault();
			var token = jQuery('meta[name="csrf-token"]').attr('content');
		});

		jQuery('#remove-user').on('show.bs.modal', function (event) {
			var button = jQuery(event.relatedTarget); // Button that triggered the modal
			var user_id = button.data('id');
			jQuery('#remove-btn').data('id',user_id);
			jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
		});

		jQuery('#remove-btn').click(function(e) {
			e.preventDefault();
			var user_id = jQuery(this).data('id');
			var token = jQuery('meta[name="csrf-token"]').attr('content');

			jQuery.ajax({  
				url: '{{ route('schedules.index') }}/'+user_id,
				type: 'POST',
				dataType: 'json',
				data: { _token: token, _method: 'DELETE' },
			})
			.always(function(data) {
				if (data.error) {
					toastr.error(data.message,'Error');
				} else {
					jQuery('#remove-user').modal('toggle');
					toastr.success(data.message,'Success');
					jQuery('#user-row-'+user_id).remove();
				}
			});
			
		});
	});
</script>
@endsection

