<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>PRINT LOGS</title>
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<style>body{font-size:12px;}</style>
	</head>
	<body>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					@if($logs->count())
					<div align="center">
						<h3>Class Attendance</h3>
						@if($school)
							<h6>{{ $school->name }}</h6>
						@endif
						@if($subject)
							<h4>{{ 'Subject: ' . $subject->name }} ( {{ $subject->description }} )</h4>
							<h4>{{ 'Teacher: ' . $subject->teacher->first_name . ' ' . $subject->teacher->last_name }}</h4>
						@endif
						@if($section)
							<h4>{{ $section->year_level.'-'.$section->name }}</h4>
						@endif
					</div>
					<hr>

					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Student</th>
								<th>School / Section</th>
								<th>Subject</th>
								<th>Room</th>
								<th>Schedule</th>
								<th>Status</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $logs as $log )
							<?php $style = ($log->present) ? 'color:#fff;background:#1ab394;': 'color:#fff;background:#ed5565;'; ?>
							<tr>
								<td style="{{ $style }}">{{ $log->student->first_name.' '.$log->student->last_name }}</td>
								<td style="{{ $style }}">{{ $log->section->year_level.' - '.$log->section->name .' ('. $log->section->school->abrevation.')'}}</td>
								<td style="{{ $style }}">{{ $log->subject->name }}</td>
								<td style="{{ $style }}">{{ $log->schedule->room }}</td>
								<td style="{{ $style }}">{{ date("h:i A", strtotime($log->schedule->time_start) ) .' - '.date("h:i A", strtotime($log->schedule->time_end) ) }}</td>
								<td style="{{ $style }}">
									@if($log->present) 
										Present  
									@else 
										Absent
									@endif
								</td>
								<td style="{{ $style }}">{{ date("l \of F j, Y h:i:s A", strtotime($log->created_at) ) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>

					@endif
					

				</div>
			</div>
		</div>
		<script>
			window.print();
		</script>
	</body>
</html>