@extends('layouts.admin')
@section('content')
<div class="col-md-6">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>{{ $subject->name }} ({{ date('l') }})<small class="m-l-sm"> </small></h5>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                    <h4>Start Time: {{ date('h:i A', strtotime($schedule->time_start)) }}</h4>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                    <h4>Dismisal Time: {{ date('h:i A', strtotime($schedule->time_end)) }}</h4>
                </div>
                <div class="col-md-12">
                    <div id="reader"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">

</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('qrscanner/js/html5-qrcode.min.js')}}"></script>
<script>
toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function onScanSuccess(studentid) {

	// handle the scanned code as you like
    console.log(`QR matched = ${studentid}`);
    $.ajax({
        type: "GET",
        url: "{{ route('api_log') }}",
        data: {student:studentid,schedule:'{{$schedule->id}}'},
        dataType: "json",
        success: function (response) {
            if (response.error) {
                toastr.error(response.message,'Error');    
            } else {
                toastr.success(response.message,'Success');
            }
        }
    });
}

function onScanFailure(error) {
	// handle scan failure, usually better to ignore and keep scanning
    toastr.error(`QR error: ${error}`,'Error');    
}

let html5QrcodeScanner = new Html5QrcodeScanner(
	"reader", { fps: 10, qrbox: 250 }, /* verbose= */ true);
html5QrcodeScanner.render(onScanSuccess, onScanFailure);


</script>
@endsection